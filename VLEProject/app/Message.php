<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{

    /**
     * Fields that are mass assignable
     *
     * @var array
     */

    use SoftDeletes;

    protected $fillable = ['message', 'breakout_room_num'];
    protected $table= 'messages';

    protected $hidden = ['user_id'];

    /**
     * A message belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
