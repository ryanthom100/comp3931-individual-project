<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Events\MessageSent;
use GuzzleHttp\Client;
use Creativeorange\Gravatar\Facades\Gravatar;

use DB;
use Illuminate\Support\Facades\Hash;
use info;

error_reporting(E_ALL);

class AdminController extends Controller
{
    public function createBreakoutRooms(Request $request){
        $numOfRooms = $request->input('inputBreakoutNum'); // get the post request
        // set the number of breakout room created into a field
        DB::table('users')->where('role','admin')->update(['num_of_rooms' => $numOfRooms]);

        //allocate rooms to users
        $userList = DB::table('users')->where('role','user')->get();
        $count = 0;
        if($userList){
            $maxCount = $numOfRooms;
            foreach($userList as $user){
                $count = $count + 1;
                DB::table('users')->where('id',$user->id)->update(['breakout_room_num' => $count]);
                if($count == $maxCount){
                    $count = 0;
                }
            }
        }
        return back(); // return to the previous view which will always be the admin dashboard
    }

    public function getAllUsersBack(Request $request)
    {
        $admin = DB::table('users')->where('role','admin')->first(); // get admin
        for($x = 1; $x <= $admin-> num_of_rooms; $x++){
            Message::where('breakout_room_num', $x)->delete();

        }
        // query to get all students and relocate them to main chatroom
        DB::table('users')->where('role','user')->update(['breakout_room_num' =>
            $request->input('getUsersBack')]);
        // set the number of breakout rooms back to 0 for the field
        DB::table('users')->where('role','admin')->update(['num_of_rooms' => 0]);



        return back(); // return to the previous view which will always be the admin dashboard
    }

    public function numOfUsersInRoom($num){
        $numOfUsers = DB::table('users')->where('breakout_room_num', $num)->count();
        return $numOfUsers;
    }

    public function addUserToBreakout(Request $request){
        // algorithm finds the breakout room with the smallest number of students
        $smallestRoom = 0;
        //get the number of breakout rooms and iterate through them to find the smallest one
        for( $x = 1; $x <= DB::table('users')->where('role','admin')->first()->num_of_rooms; $x++){
            if($x == 1){
                $smallestRoom = $x;
            }
            // numOfUsersInRoom function gets the number of students in the breakout room
            else if($this->numOfUsersInRoom($x) < $this->numOfUsersInRoom($smallestRoom)){
                $smallestRoom = $x;
            }
        }
        // relocate the selected student to the smallest breakout room
        DB::table('users')->where('role','user')->where('name', $request->input('add_user'))
            ->update(['breakout_room_num' => $smallestRoom]);
        return back(); // return to the previous view which will always be the admin dashboard
    }

}
