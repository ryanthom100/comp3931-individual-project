<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Events\MessageSent;
use GuzzleHttp\Client;
use Creativeorange\Gravatar\Facades\Gravatar;

use DB;
use Illuminate\Support\Facades\Hash;
use info;

error_reporting(E_ALL);

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    //@return \Illuminate\Http\Response gives warning
    public function index()
    {

        $roomNum = DB::table('users')->where('role','admin')->first();
        $arrRooms = [];

        // list of rooms for admin dashboard
        if( $roomNum -> num_of_rooms > 0){
            for($x = 0; $x < $roomNum -> num_of_rooms; $x++){
                $arrRooms[$x] = "Breakout Room " . strval($x+1);
            }
        }

        // list of users per room for admin dashboard
        $arrRoomsUsers = array();
        $roomUsersLimit = array(); // array that holds the limit of each array for the arrRoomUsers
        if( $roomNum -> num_of_rooms > 0){
            for($x = 0; $x < $roomNum -> num_of_rooms; $x++){
                $tempArr = DB::table('users')->where('role','user')->where('breakout_room_num',$x+1)->get();
                $y = 0;
                foreach($tempArr as $temp){
                    $arrRoomsUsers[$x][$y] =  $temp->name;
                    $y++;
                }
                $roomUsersLimit[$x] = $y;
            }
        }

        $users = DB::table('users')->where('role','user')->where('breakout_room_num',0)->get();

        $usersFromBreakout = DB::table('users')->where('role','user')
            ->where('breakout_room_num',Auth::user()->breakout_room_num)->get();

        // check if the student tried to create an admin account, ONLY 1 ADMIN ALLOWED
        if(DB::table('users')->where('role','admin')->count() > 1){
            DB::table('users')->where('id',Auth::user()->id)->delete();
            return view('auth.registerNotAllowed');
        }

        if(Auth::user()->role == "admin"){
            return view('admin_pages.admin_dashboard',['users' => $users, 'rooms' => $arrRooms, 'roomUsers'=> $arrRoomsUsers, 'roomUsersLimit'=>$roomUsersLimit]);
        }
        else if(Auth::user()->role == "user"){
            if(Auth::user()->breakout_room_num == 0){
                return view('student_pages.user_dashboard');
            }else{
                return view('student_pages.user_breakout_room',['users' => $usersFromBreakout]);
                }
            }
        }


    /**
     * Fetch all messages
     *
     * @return Message|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMessages()
    {
        return Message::with('user')->where('breakout_room_num', '=', Auth::user()->breakout_room_num)->get();
    }


    /**
     * Persist message to database
     *
     * @param Request $request
     * @return Response
     * @throws \Creativeorange\Gravatar\Exceptions\InvalidEmailException
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message'), 'breakout_room_num' => Auth::user()->breakout_room_num
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }


    public function leaveBreakout(Request $request){
        // update the breakout room number for the current user
        DB::table('users')->where('id',Auth::user()->id)->update(['breakout_room_num' =>
            $request->input('leaveRoom')]);
        // redirect to the previous view which in this case will always be the main chatroom
        return back();
    }


    public function globalMessage(Request $request){
        $admin = DB::table('users')->where('role','admin')->first(); // get admin
        //create new instances of the global message and add them to each breakout room
        for($x = 0; $x <= $admin-> num_of_rooms; $x++){
            $user = Auth::user();
            // create a new message that will be broadcast
            $message = $user->messages()->create([
                'message' => $request->input('global_message'), 'breakout_room_num' => $x
            ]);
            // broadcast each instance of the message to pusher channel
            broadcast(new MessageSent($user, $message))->toOthers();
        }
        return back(); // return to the previous view which will always be the admin dashboard
    }


}


