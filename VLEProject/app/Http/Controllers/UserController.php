<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {
    public function leaveBreakout(Request $request){
        // update the breakout room number for the current user
        DB::table('users')->where('id',Auth::user()->id)->update(['breakout_room_num' =>
            $request->input('leaveRoom')]);
        // redirect to the previous view which in this case will always be the main chatroom
        return back();
    }
}
