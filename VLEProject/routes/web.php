<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;


Auth::routes();
//
//$user = auth()->user();
//dd($user);
//


Route::get('/', 'ChatsController@index');

Route::post('/createBreakoutRooms', 'AdminController@createBreakoutRooms');
Route::post('/sendGlobalMessage', 'ChatsController@globalMessage');
Route::post('/leaveBreakout', 'ChatsController@leaveBreakout')->name("leaveBreakout");
Route::post('/getAllUsers', 'AdminController@getAllUsersBack')->name("getAllUsers");
Route::post('/addUserToRoom', 'AdminController@addUserToBreakout');
Route::get('messages', 'ChatsController@getMessages');
Route::post('messages', 'ChatsController@sendMessage');



