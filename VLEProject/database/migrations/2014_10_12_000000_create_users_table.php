<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->enum('role', array('user', 'admin'))->default('user');
            $table->Integer('breakout_room_num')->default(0); // 0 is the main chatroom
            $table->timestamp('email_verified_at')->useCurrent();
            $table->string('password');
            $table->Integer('num_of_rooms')->default(-1); // will only be allocated for the admin
            $table->string('avatar_url')->default('none');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
