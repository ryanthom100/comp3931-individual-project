<!-- resources/views/admin_dashboard.blade.php -->

@extends('layouts.adminLayout')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Chats</div>

                    <div class="panel-body">
                        <chat-messages :messages="messages" user-curr="{{ Auth::user()->breakout_room_num }}">{{ info(Auth::user()->breakout_room_num)}}</chat-messages>
                    </div>
                    <div class="panel-footer">
                        <chat-form
                            v-on:messagesent="addMessage"
                            :user="{{ Auth::user() }}"
                        ></chat-form>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h1>Student List</h1></div>
                    <br>
                    <div class="panel-body-student-list">
                        @foreach($users as $user)
                            <p>{{ $user->name }}</p>
                        @endforeach
                    </div>
                    <br>
                    <div class="panel-footer">
                        <form action="{{url('sendGlobalMessage')}}" method="POST">
                            <div class="input-group mb-3">
                                @csrf
                                <div class="input-group-append">
                                    <input type="text" name="global_message" class="form-control"
                                           placeholder="Send msg to all chatrooms" aria-label="Breakout Rooms"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="submit">Send</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="panel-footer">
                        <form action="{{url('addUserToRoom')}}" method="POST">
                            <div class="input-group mb-3">
                                @csrf
                                <div class="input-group-append">
                                    <input type="text" name="add_user" class="form-control"
                                           placeholder="Enter users name to add" aria-label="Add User"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="submit">Add</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>

            </div>
            <div class="col-md-3 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h1>Breakout Rooms</h1></div>
                    <br>
                    <div class="panel-body-student-list">
                        @for($i=0, $count = count($rooms);$i<$count;$i++)
                            <p><b>{{ $rooms[$i] }}</b></p>
                            @for($y=0; $y< $roomUsersLimit[$i]; $y++)
                                <p>{{ $roomUsers[$i][$y] }}</p>
                            @endfor
                        @endfor
                    </div>
                    <br>
                    <div class="panel-footer">
                        <form action="{{url('createBreakoutRooms')}}" method="POST">
                            <div class="input-group mb-3">
                                @csrf
                                <div class="input-group-append">
                                    <input type="number" name="inputBreakoutNum" class="form-control"
                                           placeholder="Num of breakout rooms" aria-label="Breakout Rooms"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="submit">Create</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="panel-footer">

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Get All Users Back
                        </button>


                    </div>

                    <br>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you get all users back? The will force every user to leave a breakout room and return to
                        the global chatroom. This will also remove all breakout rooms. </p>
                </div>
                <div class="modal-footer">
                    <form action="{{route('getAllUsers')}}" method="POST">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <input name="getUsersBack" value="0" type="hidden">
                                <button class="btn btn-outline-primary" type="submit">CONFIRM</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection



<!-- <form action="{{route('leaveBreakout')}}" method = "POST">
                           @csrf
    <div class="input-group mb-3">
        <div class="input-group-append">
            <input name="leaveRoom" value="0" type="hidden">
            <button class="btn btn-outline-primary" type="submit">Leave Room</button>
        </div>
    </div>
</form> -->





