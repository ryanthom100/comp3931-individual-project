
<!-- resources/views/user_breakout_room.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Chats</div>

                    <div class="panel-body">
                        <chat-messages :messages="messages" user-curr="{{ Auth::user()->breakout_room_num }}">{{ info(Auth::user()->breakout_room_num)}}</chat-messages>

                    </div>
                    <div class="panel-footer">
                        <chat-form
                            v-on:messagesent="addMessage"
                            :user="{{ Auth::user() }}"
                        ></chat-form>
                    </div>
                </div>
            </div>


            <div class="col-md-4 col-md-offset-2">
                <div class="panel-heading"><h1>Students in this Breakout Room</h1></div>
                <br>
                <div class="panel-body-student-list">
                    @foreach($users as $user)
                        <p>{{ $user->name }}</p>
                    @endforeach
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-footer">

                        <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Leave Room
</button>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



<!-- <form action="{{route('leaveBreakout')}}" method = "POST">
                           @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <input name="leaveRoom" value="0" type="hidden">
                                    <button class="btn btn-outline-primary" type="submit">Leave Room</button>
                                </div>
                            </div>
                        </form> -->


                        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <p>Are you sure you want to leave? You will be redirected back to the main room. </p>
      </div>
      <div class="modal-footer">
           <form action="{{route('leaveBreakout')}}" method = "POST">
                           @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <input name="leaveRoom" value="0" type="hidden">
                                    <button class="btn btn-outline-primary" type="submit">CONFIRM</button>
                                </div>
                            </div>
                        </form>
      </div>
    </div>
  </div>
</div>
