This repository contains the code for the software prototype created in the project 'A VLE for the New Normal'. The software prototype is a web application called 'VLE Chatroom'. Which allows a user to register and log in as a student or teacher. The user will have access to a dashboard for their corresponding role. Where they can access a variety of different functions allowing them to communicate with another user. Please NOTE that you need to refresh the web page to see new messages due to a bug.

Downloading and installing Laravel:
1) Start by installing PHP on the machine.

2) Go to this link and download and install composer:
https://getcomposer.org/doc/00-intro.md

3) Then, run the command "composer global require "laravel/installer=~1.1" from a command line


Download the VLE Chatroom web application:

1) Download a zip file of Gitlab repository
2) Unzip to a desired directory
3) cd to the project folder 'VLEProject' in this desired directory
4) run the command 'composer update'
5) ensure you have MySQL downloaded and installed
6) create a new database called 'VLEProjectDB'
7) run the command 'php artisan serve' from the folder 'VLEProject'
